package com.mycompany.shapecal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblside = new JLabel("side:",JLabel.TRAILING);
        lblside.setSize(50,20);
        lblside.setLocation(5,5);
        lblside.setBackground(Color.WHITE);
        lblside.setOpaque(true);
        frame.add(lblside);
        
        JLabel lblbase = new JLabel("base:",JLabel.TRAILING);
        lblbase.setSize(50,20);
        lblbase.setLocation(5,19);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        frame.add(lblbase);
        
        final JTextField txtside = new JTextField();
        txtside.setSize(50,20);
        txtside.setLocation(60, 5);
        frame.add(txtside);
        
        final JTextField txtbase = new JTextField();
        txtbase.setSize(50,20);
        txtbase.setLocation(60, 20);
        frame.add(txtbase);
        
        JButton bthCalculate = new JButton("Calculate");
        bthCalculate.setSize(100,20);
        bthCalculate.setLocation(120, 5);
        frame.add(bthCalculate);
        
        final JLabel lblResult = new JLabel("Triangle Width&Longs=???area=???perimeter=???");
        lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.red);
        lblResult.setOpaque(true);
        frame.add(lblResult);
            //Event Driven
        bthCalculate.addActionListener(new ActionListener(){//Anoymous class       
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strside = txtside.getText();
                String strbase = txtbase.getText();
                double side = Double.parseDouble(strside);
                double base = Double.parseDouble(strbase);                
               Triangle triangle = new Triangle(side,base);
                lblResult.setText("Triangle ="+String.format("%.2f",triangle.getSide())
                        +String.format("%.2f",triangle.getBase())
                        +"area="+String.format("%.2f",triangle.calArea())
                        +"perimeter="+String.format("%.2f",triangle.callPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showConfirmDialog(frame, "Error:plase input number","Error!!!"
                            , JOptionPane.ERROR_MESSAGE);
                    txtside.setText("");
                    txtbase.setText("");
                    txtside.requestFocus();
                }
            }
    });
            
        
        frame.setVisible(true);
    }


    }

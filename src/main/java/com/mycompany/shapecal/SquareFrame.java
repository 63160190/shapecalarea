/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapecal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class SquareFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblside = new JLabel("side:", JLabel.TRAILING);
        lblside.setSize(50, 20);
        lblside.setLocation(5, 5);
        lblside.setBackground(Color.WHITE);
        lblside.setOpaque(true);
        frame.add(lblside);

        final JTextField txtside = new JTextField();
        txtside.setSize(50, 20);
        txtside.setLocation(60, 5);
        frame.add(txtside);

        JButton bthCalculate = new JButton("Calculate");
        bthCalculate.setSize(100, 20);
        bthCalculate.setLocation(120, 5);
        frame.add(bthCalculate);

        final JLabel lblResult = new JLabel("Square side=???area=???perimeter=???");
        lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.red);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        //Event Driven
        bthCalculate.addActionListener(new ActionListener() {//Anoymous class       
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtside.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lblResult.setText("Square  =" + String.format("%.2f", square.getSide())
                            + "area=" + String.format("%.2f", square.calArea())
                            + "perimeter=" + String.format("%.2f", square.callPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showConfirmDialog(frame, "Error:plase input number", "Error!!!",
                             JOptionPane.ERROR_MESSAGE);
                    txtside.setText("");
                    txtside.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}

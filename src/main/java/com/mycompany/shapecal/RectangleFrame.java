package com.mycompany.shapecal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.shapecal.Rectangle;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWidth = new JLabel("Width:",JLabel.TRAILING);
        lblWidth.setSize(50,20);
        lblWidth.setLocation(5,5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);
        
        JLabel lblLongs = new JLabel("Longs:",JLabel.TRAILING);
        lblLongs.setSize(50,20);
        lblLongs.setLocation(5,19);
        lblLongs.setBackground(Color.WHITE);
        lblLongs.setOpaque(true);
        frame.add(lblLongs);
        
        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50,20);
        txtWidth.setLocation(60, 5);
        frame.add(txtWidth);
        
        final JTextField txtLongs = new JTextField();
        txtLongs.setSize(50,20);
        txtLongs.setLocation(60, 20);
        frame.add(txtLongs);
        
        JButton bthCalculate = new JButton("Calculate");
        bthCalculate.setSize(100,20);
        bthCalculate.setLocation(120, 5);
        frame.add(bthCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle Width&Longs=???area=???perimeter=???");
        lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.red);
        lblResult.setOpaque(true);
        frame.add(lblResult);
            //Event Driven
        bthCalculate.addActionListener(new ActionListener(){//Anoymous class       
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strWidth = txtWidth.getText();
                String strLongs = txtLongs.getText();
                double Width = Double.parseDouble(strWidth);
                double Longs = Double.parseDouble(strLongs);                
                Rectangle rectangle = new Rectangle(Width,Longs);
                lblResult.setText("Rectangle ="+String.format("%.2f",rectangle.getWidth())
                        +String.format("%.2f",rectangle.getLongs())
                        +"area="+String.format("%.2f",rectangle.calArea())
                        +"perimeter="+String.format("%.2f",rectangle.callPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showConfirmDialog(frame, "Error:plase input number","Error!!!"
                            , JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtLongs.setText("");
                    txtWidth.requestFocus();
                }
            }
    });
            
        
        frame.setVisible(true);
    }
}
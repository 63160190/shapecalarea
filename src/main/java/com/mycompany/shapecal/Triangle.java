/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapecal;

/**
 *
 * @author User
 */
public class Triangle extends Shape{
    private double side;
    private double base;
    
    public Triangle(double side,double base) {        
        super("Triangle");       
        this.side=side;
        this.base=base;
        
    }


    public double getSide() {
        return side;
    }

    public double getBase() {
        return base;
    }
    

    public void setSide(double side) {
        this.side = side;
    }

    public void setBase(double base) {
        this.base = base;
    }
    
    
    @Override
    public double calArea() {
       return 0.5*base*side; 
    }

    @Override
    public double callPerimeter() {
       return side+side+base;
    }
    
}

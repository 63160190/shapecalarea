/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapecal;

/**
 *
 * @author User
 */
public class Rectangle extends Shape{
    private double Width;
    private double Longs;
    public Rectangle(double width,double longs ) {
        super("Rectangle");
        this.Width = width;
        this.Longs = longs;
    }

    public double getWidth() {
        return Width;
    }

    public double getLongs() {
        return Longs;
    }

    public void setWidth(double Width) {
        this.Width = Width;
    }

    public void setLongs(double Longs) {
        this.Longs = Longs;
    }
    

    @Override
    public double calArea() {
       return Width*Longs;
    }

    @Override
    public double callPerimeter() {
       return 2*(Width*Longs);
    }
    
}
